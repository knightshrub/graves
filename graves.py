#!/usr/bin/env python

#    Copyright (C) 2021 - 2024  Y. Ritterbusch
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from skyfield import api, almanac
from itertools import islice
from datetime import datetime, date, time, timedelta, timezone
import numpy as np
from collections import namedtuple
import re
import click
import sys

# This namedtuple holds the object information for a single pass
Pass = namedtuple("Pass", ["stime", "salt", "saz", "etime", "ealt", "eaz"])

# convert azimuth in degrees to cardinal and intercardinal directions
def deg2cd(deg):
    deg = float(deg % 360)
    if 22.5 <= deg < 67.5:
        return "NE"
    elif 67.5 <= deg < 112.5:
        return "E"
    elif 112.5 <= deg < 157.5:
        return "SE"
    elif 157.5 <= deg < 202.5:
        return "S"
    elif 202.5 <= deg < 247.5:
        return "SW"
    elif 247.5 <= deg < 292.5:
        return "W"
    elif 292.5 <= deg < 337.5:
        return "NW"
    else:
        return "N"

# convert a four or six digit Maidenhead locator like JO61ua to latitude and longitude
def maidenhead2latlon(loc):
    # validate using a regex, I know I know, zip it Jimmy
    if re.compile("[A-R]{2}[0-9]{2}([a-x]{2})?").fullmatch(loc) is not None:
        # basic four digit locator
        lat = -90.0 + (ord(loc[1]) - ord('A')) * 10.0 + float(loc[3])
        lon = -180.0 + (ord(loc[0]) - ord('A')) * 20.0 + 2.0 * float(loc[2])
        # handle subsquare
        if len(loc) > 4:
            lat += (ord(loc[5]) - ord('a'))/24.0
            lon += (ord(loc[4]) - ord('a'))/12.0

        # shift the result to the center of the grid square depending
        # on the precision of the given square
        if len(loc) == 4:
            lat += 0.5
            lon += 1.0
        elif len(loc) == 6:
            lat += 1.25/60
            lon += 2.5/60

        return (lat, lon)
    else:
        raise ValueError(f"Invalid Maidenhead locator: {loc}")

# Print all passes in the array in a nice table
def print_table(passes, tzinfo=timezone.utc):
    print(78*'-')
    print("{0:38s}  {1:38s}".format("Start", "End"))
    subhead = "{0:24s}  {1:3s}  {2:7s}".format("Time", "Alt", "Az")
    print(f"{subhead}  {subhead}")
    print(78*'-')
    for p in passes:
        stime = p.stime.astimezone(tzinfo).strftime("%Y-%m-%d %H:%M:%S %Z")
        etime = p.etime.astimezone(tzinfo).strftime("%Y-%m-%d %H:%M:%S %Z")
        print(f"{stime:24s}  {p.salt:2.0f}°  {p.saz:3.0f}° {deg2cd(p.saz):2s}  {etime:24s}  {p.ealt:2.0f}°  {p.eaz:3.0f}° {deg2cd(p.eaz):2s}")
    print(78*'-')

# Return a function of time that can be used by the almanac computation to check
# that the object is in the correct altitude and azimuth range for illumination
# by the GRAVES transmitter
def illuminated_by_graves(obj, ephemeris):
    # GRAVES transmitter site
    transmitter = api.wgs84.latlon(47.348, 5.5151)
    # the transmitter location
    tx = ephemeris["Earth"] + transmitter

    def f(t):
        alt, az, d = tx.at(t).observe(obj).apparent().altaz()
        return np.array([15 <= alti <= 40 and 90 <= azi <= 270 for alti, azi in zip(alt.degrees, az.degrees)])

    f.step_days = 1/(24*60)
    return f

def find_passes(obj, obs, ephemeris, start, end):
    ts = api.load.timescale()
    t0 = ts.from_datetime(start)
    t1 = ts.from_datetime(end)

    obj_illuminated = illuminated_by_graves(obj, ephemeris)

    # find rise and set times
    times, riseset = almanac.find_discrete(t0, t1, obj_illuminated)

    observer = ephemeris["Earth"] + obs

    utcnow = datetime.now(timezone.utc)

    if len(times) > 0 and len(riseset) > 0:
        # find the first obj rise
        firstrise = (riseset==1).argmax()
        # iterate in pairs over rise and set time
        passes = []
        for risetime, settime in zip(islice(times, firstrise, None, 2), islice(times, firstrise+1, None, 2)):
            def object_position(time):
                alt, az, d = observer.at(time).observe(obj).apparent().altaz()
                t = time.utc_datetime()
                return (t, alt.degrees, az.degrees, d)

            stime, salt, saz, _ = object_position(risetime)
            etime, ealt, eaz, _ = object_position(settime)

            # only return passes that haven't ended yet
            if (etime - utcnow) > timedelta():
                passes.append(Pass(stime, salt, saz, etime, ealt, eaz))

        return passes

@click.command()
@click.argument("locator")
@click.option("-u", "--utc", is_flag=True, help="Display times in UTC instead of system local time")
@click.option("-d", "--days", default=14, help="Give pass predictions for number of days")
def run(locator, utc, days):
    """Print a table of lunar passes where echoes from the GRAVES radar
    can be received in the location given by the 4- or 6-digit Maidenhead LOCATOR.
    """
    ephemeris = api.load("de421.bsp")

    # local date at midnight with timezone UTC
    now = datetime.combine(date.today(), time(), timezone.utc)
    fut = now + timedelta(days=days)

    # receiver site
    try:
        lat, lon = maidenhead2latlon(locator)
    except ValueError as e:
        print(e)
        sys.exit(1)

    receiver = api.wgs84.latlon(lat, lon)

    passes = find_passes(ephemeris["Moon"], receiver, ephemeris, now, fut)

    if passes is not None:
        tzinfo = timezone.utc if utc else now.astimezone().tzinfo
        print_table(passes, tzinfo=tzinfo)
    else:
        print("No passes found!")

if __name__ == "__main__":
    run()
