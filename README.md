# graves.py

Predict when the Moon is illuminated by the French [GRAVES](https://en.wikipedia.org/wiki/Graves_%28system%29)
space surveillance radar.
Show the beginning and end of illumination time together with apparent Moon
position in the sky at the observer's location given by a 4- or 6-digit 
[Maidenhead locator](https://en.wikipedia.org/wiki/Maidenhead_Locator_System).

A 6-digit Maidenhead locator should be accurate enough for the purposes of 
determining when a radar echo can be received at 143.05 MHz.
It allows easy specification of a location on the surface of Earth without
entering annoying latitude / longitude pairs.

The tool depends on the following *wonderful* Python libraries:
- [Skyfield](https://rhodesmill.org/skyfield/)
- [numpy](https://numpy.org/)
- [click](https://click.palletsprojects.com/)

The rest is all part of the Python standard library.

This project is free software, and you are welcome to redistribute it under
certain conditions; see LICENSE for details.

# Set up

It is generally recommended to work in Python virtual environments, this way
version conflicts between `graves.py` and a system wide Python environment
can be avoided.
In order to set up a self-contained virtual environment for `graves.py`,
change into the newly cloned directory and run the following commands:
```sh
$ python -m venv env
$ source env/bin/activate
$ pip install -r requirements.txt
```

# Examples

```
$ python graves.py --help
Usage: graves.py [OPTIONS] LOCATOR

  Print a table of lunar passes where echoes from the GRAVES radar can be
  received in the location given by the 4- or 6-digit Maidenhead LOCATOR.

Options:
  -u, --utc           Display times in UTC instead of system local time
  -d, --days INTEGER  Give pass predictions for number of days
  --help              Show this message and exit.
```

```
$ python graves.py -d 5 JO61ua
------------------------------------------------------------------------------
Start                                   End                                   
Time                      Alt  Az       Time                      Alt  Az     
------------------------------------------------------------------------------
2024-04-13 22:12:34 CEST  35°  273° W   2024-04-13 22:13:41 CEST  35°  273° W 
2024-04-14 14:50:56 CEST  44°  100° E   2024-04-14 14:54:39 CEST  45°  101° E 
2024-04-14 23:06:29 CEST  35°  272° W   2024-04-14 23:14:06 CEST  33°  273° W 
2024-04-15 15:35:49 CEST  42°  100° E   2024-04-15 15:55:49 CEST  45°  104° E 
2024-04-15 23:50:54 CEST  34°  268° W   2024-04-16 00:19:13 CEST  30°  274° W 
2024-04-16 16:10:45 CEST  37°   99° E   2024-04-16 16:58:50 CEST  44°  110° E 
2024-04-17 00:25:53 CEST  34°  263° W   2024-04-17 01:25:36 CEST  25°  274° W 
2024-04-17 16:38:22 CEST  31°   99° E   2024-04-17 18:03:26 CEST  44°  118° SE
------------------------------------------------------------------------------
```
